//
//  main.m
//  SelectionSort
//
//  Created by Angel Lee on 2016-02-13.
//  Copyright © 2016 Angel Lee. All rights reserved.
//

#import <Foundation/Foundation.h>

NSArray *selectionSort(NSArray *arr);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        // insert code here...
        NSLog(@"Hello, World!");
    }
    NSArray *arr = selectionSort(@[@23, @42, @4, @16, @8, @15]);

    return 0;
}

NSArray *selectionSort(NSArray *arr) {
    NSMutableArray *m_arr = [NSMutableArray arrayWithArray:arr];
    
    for (int i=0; i<m_arr.count; i++) {
        int currentMinIndex = i;
        for (int j=i+1; j<m_arr.count; j++) {
            if (m_arr[currentMinIndex] > m_arr[j]) {
                currentMinIndex = j;
            }
        }
        NSNumber *temp = m_arr[i];
        m_arr[i] = m_arr[currentMinIndex];
        m_arr[currentMinIndex] = temp;
        
    }
    
    return [m_arr copy];
}
